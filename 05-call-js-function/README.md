# 05-call-js-function

## Requirements

- Create a `go.mod` file
- Create a `main.go` file

## go.mod

```text
module 05-call-js-function

go 1.17
```

## main.go

```golang
package main

import (
	"fmt"
	"syscall/js"
)

func main() {
	// Call JavaScript function
	fmt.Println("[From Go]", js.Global().Call("sayHello", "Bill"))

	// Get JavaScript value
	message := js.Global().Get("message").String()
	fmt.Println("[From Go]","message (before):", message)

	// Change the JavaScript value
	js.Global().Set("message", "😉 Hello from Go")

	// Get JavaScript value
	bill := js.Global().Get("bill")
	fmt.Println("[From Go]","bill (before):", bill)

	// Change the JavaScript values
	bill.Set("firstName", "Bill")
	bill.Set("lastName", "Ballantine")

	<-make(chan bool)
}

```

## Build, run, ...

```bash
# build the wasm fil
GOOS=js GOARCH=wasm go build -o main.wasm
```

```bash
# run the http server
node index.js
```

- Open the webpage in your browser
- Open the console (developer tools)

## Add this function on the JavaScript side

```javascript
function sayHelloToHuman(human) {
	return `👋 I'm a JavaScript Function: Hello ${human.firstName} ${human.lastName}`
}
```

And call it from Go:

```golang
fmt.Println("[From Go]", js.Global().Call("sayHelloToHuman", human))
```

👋 **Tips**: use `map[string]interface{}` to define `human`